// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_PDELAB_LOCALOPERATOR_JACOBIANAPPLYHELPER_HH
#define DUNE_PDELAB_LOCALOPERATOR_JACOBIANAPPLYHELPER_HH

namespace Dune{
  namespace PDELab{

    //======================
    // Jacobian Apply Volume
    //======================

    template <typename LOP, typename EG, typename LFSU, typename X, typename LFSV, typename Y>
    std::enable_if_t<LOP::isLinear> jacobianApplyVolume(
      const LOP& lop,
      const EG& eg,
      const LFSU& lfsu, const X& z, const LFSV& lfsv,
      Y& y)
    {
      lop.jacobian_apply_volume(eg, lfsu, z, lfsv, y);
    }
    template <typename LOP, typename EG, typename LFSU, typename X, typename Z, typename LFSV, typename Y>
    std::enable_if_t<LOP::isLinear> jacobianApplyVolume(
      const LOP& lop,
      const EG& eg,
      const LFSU& lfsu, const X& x, const Z& z, const LFSV& lfsv,
      Y& y)
    {
      DUNE_THROW(Dune::Exception, "You try to call a nonlinear method on a linear operator");
    }
    template <typename LOP, typename EG, typename LFSU, typename X, typename LFSV, typename Y>
    std::enable_if_t<not LOP::isLinear> jacobianApplyVolume(
      const LOP& lop,
      const EG& eg,
      const LFSU& lfsu, const X& z, const LFSV& lfsv,
      Y& y)
    {
      DUNE_THROW(Dune::Exception, "You try to call a linear method on a nonlinear operator");
    }
      template <typename LOP, typename EG, typename LFSU, typename X, typename Z, typename LFSV, typename Y>
    std::enable_if_t<not LOP::isLinear> jacobianApplyVolume(
      const LOP& lop,
      const EG& eg,
      const LFSU& lfsu, const X& x, const Z& z, const LFSV& lfsv,
      Y& y)
    {
      lop.jacobian_apply_volume(eg, lfsu, x, z, lfsv, y);
    }

    //========================
    // Jacobian Apply Skeleton
    //========================

    template <typename LOP, typename IG, typename LFSU, typename X, typename LFSV, typename Y>
    std::enable_if_t<LOP::isLinear> jacobianApplySkeleton(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& z_s, const LFSV& lfsv_s,
      const LFSU& lfsu_n, const X& z_n, const LFSV& lfsv_n,
      Y& y_s, Y& y_n)
    {
      lop.jacobian_apply_skeleton(ig, lfsu_s, z_s, lfsv_s, lfsu_n, z_n, lfsv_n, y_s, y_n);
    }
    template <typename LOP, typename IG, typename LFSU, typename X, typename Z, typename LFSV, typename Y>
    std::enable_if_t<LOP::isLinear> jacobianApplySkeleton(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& x_s, const Z& z_s, const LFSV& lfsv_s,
      const LFSU& lfsu_n, const X& x_n, const Z& z_n, const LFSV& lfsv_n,
      Y& y_s, Y& y_n)
    {
      DUNE_THROW(Dune::Exception, "You try to call a nonlinear method on a linear operator");
    }
    template <typename LOP, typename IG, typename LFSU, typename X, typename LFSV, typename Y>
    std::enable_if_t<not LOP::isLinear> jacobianApplySkeleton(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& z_s, const LFSV& lfsv_s,
      const LFSU& lfsu_n, const X& z_n, const LFSV& lfsv_n,
      Y& y_s, Y& y_n)
    {
      DUNE_THROW(Dune::Exception, "You try to call a nonlinear method on a linear operator");
    }
      template <typename LOP, typename IG, typename LFSU, typename X, typename Z, typename LFSV, typename Y>
    std::enable_if_t<not LOP::isLinear> jacobianApplySkeleton(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& x_s, const Z& z_s, const LFSV& lfsv_s,
      const LFSU& lfsu_n, const X& x_n, const Z& z_n, const LFSV& lfsv_n,
      Y& y_s, Y& y_n)
    {
      lop.jacobian_apply_skeleton(ig, lfsu_s, x_s, z_s, lfsv_s, lfsu_n, x_n, z_n, lfsv_n, y_s, y_n);
    }

    //========================
    // Jacobian Apply Boundary
    //========================

    template<typename LOP, typename IG, typename LFSU, typename X, typename LFSV, typename Y>
    std::enable_if_t<LOP::isLinear> jacobianApplyBoundary(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& z_s, const LFSV& lfsv_s,
      Y& y_s)
    {
      lop.jacobian_apply_boundary(ig, lfsu_s, z_s, lfsv_s, y_s);
    }
    template<typename LOP, typename IG, typename LFSU, typename X, typename Z, typename LFSV, typename Y>
    std::enable_if_t<LOP::isLinear> jacobianApplyBoundary(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& x_s, const Z& z_s, const LFSV& lfsv_s,
      Y& y_s)
    {
      DUNE_THROW(Dune::Exception, "You try to call a nonlinear method on a linear operator");
    }
    template<typename LOP, typename IG, typename LFSU, typename X, typename LFSV, typename Y>
    std::enable_if_t<not LOP::isLinear> jacobianApplyBoundary(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& z_s, const LFSV& lfsv_s,
      Y& y_s)
    {
      DUNE_THROW(Dune::Exception, "You try to call a nonlinear method on a linear operator");
    }
      template<typename LOP, typename IG, typename LFSU, typename X, typename Z, typename LFSV, typename Y>
    std::enable_if_t<not LOP::isLinear> jacobianApplyBoundary(
      const LOP& lop,
      const IG& ig,
      const LFSU& lfsu_s, const X& x_s, const Z& z_s, const LFSV& lfsv_s,
      Y& y_s)
    {
      lop.jacobian_apply_boundary(ig, lfsu_s, x_s, z_s, lfsv_s, y_s);
    }

  }
}


#endif
