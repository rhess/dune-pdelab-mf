// -*- tab-width: 2; indent-tabs-mode: nil -*-
// vi: set et ts=2 sw=2 sts=2:
#ifndef DUNE_PDELAB_LOCALOPERATOR_DEBUGJACOBIANAPPLY_HH
#define DUNE_PDELAB_LOCALOPERATOR_DEBUGJACOBIANAPPLY_HH

#include <dune/pdelab/localoperator/blockdiagonalwrapper.hh>

namespace Dune {
  namespace PDELab {

    /* Test the jacobian apply method by comparing to assembling the jacobian
     * and doing matrix vector multiplication.
     */
    template <typename GridOperator>
    void testJacobianApply(const GridOperator& gridOperator){
      using Dune::PDELab::Backend::native;
      using Domain = typename GridOperator::Domain;
      using Range = typename GridOperator::Range;
      using Jacobian = typename GridOperator::Jacobian;

      Domain input(gridOperator.trialGridFunctionSpace());
      input = 1.0;

      Jacobian jacobian(gridOperator);
      gridOperator.jacobian(input, jacobian);

      Domain apply(gridOperator.trialGridFunctionSpace());
      apply = 0.0;

      // Iterate over blocks and indices in block
      for (std::size_t i=0; i<native(apply).size(); ++i){
        for (std::size_t j=0; j<native(apply)[0].size(); ++j){
          apply = 0.0;
          native(apply)[i][j] = 1.0;

          // Matrix-based operator application
          Range result(gridOperator.testGridFunctionSpace());
          native(jacobian).mv(native(apply), native(result));

          // Matrix-free operator application
          Range result_2(gridOperator.testGridFunctionSpace());
          if (GridOperator::LocalAssembler::LocalOperator::isLinear)
            gridOperator.jacobian_apply(apply, result_2);
          else
            gridOperator.jacobian_apply(input, apply, result_2);


          native(result) -= native(result_2);
          auto zero = native(result).two_norm();
          if (zero > 1e-12){
            DUNE_THROW(Dune::Exception, "Error in operator application");
          }
        }
      }
    }

  } // namespace PDELab
} // namespace Dune

#endif
